#pragma once

namespace ProjectHW {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Details
	/// </summary>
	public ref class Details : public System::Windows::Forms::Form
	{
	public:
		Details(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Details()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^ ������;
	protected:
	private: System::Windows::Forms::VScrollBar^ vScrollBar1;

	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::HScrollBar^ hScrollBar1;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->������ = (gcnew System::Windows::Forms::ListBox());
			this->vScrollBar1 = (gcnew System::Windows::Forms::VScrollBar());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->hScrollBar1 = (gcnew System::Windows::Forms::HScrollBar());
			this->SuspendLayout();
			// 
			// ������
			// 
			this->������->FormattingEnabled = true;
			this->������->ItemHeight = 16;
			this->������->Location = System::Drawing::Point(17, 16);
			this->������->Margin = System::Windows::Forms::Padding(4);
			this->������->Name = L"������";
			this->������->Size = System::Drawing::Size(344, 244);
			this->������->TabIndex = 0;
			// 
			// vScrollBar1
			// 
			this->vScrollBar1->Location = System::Drawing::Point(339, 16);
			this->vScrollBar1->Name = L"vScrollBar1";
			this->vScrollBar1->Size = System::Drawing::Size(17, 245);
			this->vScrollBar1->TabIndex = 1;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(246, 280);
			this->button1->Margin = System::Windows::Forms::Padding(4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(115, 28);
			this->button1->TabIndex = 3;
			this->button1->Text = L"�����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Details::button1_Click);
			// 
			// hScrollBar1
			// 
			this->hScrollBar1->Location = System::Drawing::Point(17, 239);
			this->hScrollBar1->Name = L"hScrollBar1";
			this->hScrollBar1->Size = System::Drawing::Size(321, 17);
			this->hScrollBar1->TabIndex = 2;
			// 
			// Details
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(379, 321);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->hScrollBar1);
			this->Controls->Add(this->vScrollBar1);
			this->Controls->Add(this->������);
			this->Margin = System::Windows::Forms::Padding(4);
			this->Name = L"Details";
			this->Text = L"������";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e);
	};
}
