#include "Functhion.h"

string& Convert_String_to_string(String^ _str, string& ostr) {
	using namespace Runtime::InteropServices;
	const char* chars = (const char*)Marshal::StringToHGlobalAnsi((_str)).ToPointer();
	ostr = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	return ostr;
}

String^ Convert_string_to_String(string& str, String^ _ostr) {
	_ostr = gcnew System::String(str.c_str());
	return _ostr;
}

string& Convert_String_to_string(String^ _str) {
	string ostr;
	using namespace Runtime::InteropServices;
	const char* chars = (const char*)Marshal::StringToHGlobalAnsi((_str)).ToPointer();
	ostr = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	return ostr;
}

String^ Convert_string_to_String(string& str) {
	System::String^ _ostr = gcnew System::String(str.c_str());
	return _ostr;
}
