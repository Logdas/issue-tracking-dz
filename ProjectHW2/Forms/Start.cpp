#include "Start.h"
#include "Authorizathion.h"
#include "Details.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThreadAttribute]
void main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	ProjectHW::Start form;
	Application::Run(% form);
}

System::Void ProjectHW::Start::button1_Click(System::Object^ sender, System::EventArgs^ e) {
	Authorizathion^ form = gcnew Authorizathion();
	this->Hide();
	form->Show();
}

System::Void ProjectHW::Start::button2_Click(System::Object^ sender, System::EventArgs^ e) {
	Details^ form = gcnew Details();
	this->Hide();
	form->Show();
}

System::Void ProjectHW::Start::button3_Click(System::Object^ sender, System::EventArgs^ e) {
	Application::Exit();
}
