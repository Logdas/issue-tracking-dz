#pragma once

namespace ProjectHW {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� View
	/// </summary>
	public ref class View : public System::Windows::Forms::Form
	{
	public:
		View(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~View()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::ListBox^ ������;
	private: System::Windows::Forms::VScrollBar^ vScrollBar1;
	private: System::Windows::Forms::HScrollBar^ hScrollBar1;
	private: System::Windows::Forms::Button^ button1;
	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->������ = (gcnew System::Windows::Forms::ListBox());
			this->vScrollBar1 = (gcnew System::Windows::Forms::VScrollBar());
			this->hScrollBar1 = (gcnew System::Windows::Forms::HScrollBar());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(78, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"���� ������:";
			// 
			// ������
			// 
			this->������->FormattingEnabled = true;
			this->������->Location = System::Drawing::Point(12, 16);
			this->������->Name = L"������";
			this->������->Size = System::Drawing::Size(260, 199);
			this->������->TabIndex = 1;
			// 
			// vScrollBar1
			// 
			this->vScrollBar1->Location = System::Drawing::Point(255, 16);
			this->vScrollBar1->Name = L"vScrollBar1";
			this->vScrollBar1->Size = System::Drawing::Size(17, 199);
			this->vScrollBar1->TabIndex = 2;
			// 
			// hScrollBar1
			// 
			this->hScrollBar1->Location = System::Drawing::Point(13, 197);
			this->hScrollBar1->Name = L"hScrollBar1";
			this->hScrollBar1->Size = System::Drawing::Size(242, 17);
			this->hScrollBar1->TabIndex = 3;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(197, 226);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 4;
			this->button1->Text = L"����";
			this->button1->UseVisualStyleBackColor = true;
			// 
			// View
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->hScrollBar1);
			this->Controls->Add(this->vScrollBar1);
			this->Controls->Add(this->������);
			this->Controls->Add(this->label1);
			this->Name = L"View";
			this->Text = L"��������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}
