#include "Detail.h"

Detail::Detail() {
	m_name = "-";
	m_type = Type::NO_TYPE;
}

Detail::Detail(string name) {
	m_name = name;
	m_type = Type::NO_TYPE;
}

Detail::Detail(string name, Type type) {
	m_name = name;
	m_type = type;
}


Detail::~Detail() {

}

ostream& operator<<(ostream& out, Detail& detail) {
	out << "�������� ������: " << detail.m_name << "\n"
		<< "���: ";
	if (detail.m_type == Type::NO_TYPE) cout << "No type";
	return out;
}

istream& operator>>(istream& in, Detail& detail) {
	cout << "������� �������� ������: "; 
	in >> detail.m_name;
	cout << "�������� ��� ������: \n";
	cout << "0. No type\n";

	in >> detail.m_type;
	return in;
}
