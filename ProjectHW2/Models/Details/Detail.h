#pragma once
#include <string>
#include <iostream>
#include "DetailsType.h"
using namespace std;

class Detail {
public:
	Detail();
	Detail(string name);
	Detail(string name, Type type);
	~Detail();

	friend ostream& operator<<(ostream& out, Detail& detail);
	friend istream& operator>>(istream& in,  Detail& detail);
private:
	string m_name;
	int    m_type;
};

