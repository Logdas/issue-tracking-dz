#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Human {
public:
	static int s_idGenerator;
	//static int s_countPeople;

	Human();
	Human(string firstName, string lastName, string login, string m_password);
	Human(string login, string m_password);
	Human(const Human&);
	~Human();

	void Print() const;
	void set();

	Human& operator=(const Human&);
	bool   operator>(const Human&);
	bool   operator==(const Human& human);

	friend ostream& operator<<(ostream& out, Human& human);
	friend istream& operator>>(istream& in, Human& human);

	string getLogin();
	string getPassword();

	void   setLogin(string login);
	void   setPassword(string password);
	void   setID(int id);


protected:
	string m_firstName;
	string m_lastName;
	string m_login;
	string m_password;

	int m_id;
};