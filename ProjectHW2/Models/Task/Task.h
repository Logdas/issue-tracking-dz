#pragma once
#include <string>
#include <iostream>
#include "../Humans/Human.h"
using namespace std;

class Task {
public:
	Task();
	Task(string title, string name_task, Human human);
	~Task();

	void SetHuman(Human human);
	Human GetHuman();
	void setStatus(bool status);
	bool getStatus();

	friend ostream& operator<<(ostream& out, Task& task);
	friend istream& operator>>(istream& in,  Task& task);
private:
	string m_title;
	string m_text;
	Human  m_human;
	bool   m_status;
};


