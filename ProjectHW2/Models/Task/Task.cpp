#include "Task.h"

Task::Task() {
	m_title = "-";
	m_text = "-";
	m_human.setLogin("-");
	m_status = false;
}

Task::Task(string title, string name, Human human) {
	m_title = 
	m_text = name;
	m_human = human;
	m_status = false;
}

Task::~Task() {

}

void Task::SetHuman(Human human) {
	m_human = human;
}

Human Task::GetHuman() {
	return m_human;
}

void Task::setStatus(bool status) {
	m_status = status;
}

bool Task::getStatus() {
	return m_status;
}

ostream& operator<<(ostream& out, Task& task) {
	out << task.m_title << endl << task.m_text << endl
		<< "�����������: " << task.m_human.getLogin() << endl
		<< "������: " << task.m_status << endl;
	return out;
}

istream& operator>>(istream& in, Task& task) {
	cout << "������� ��������� ������: ";
	in >> task.m_title;
	cout << "������� ���� ������: ";
	in >> task.m_text;
	return in;
}
