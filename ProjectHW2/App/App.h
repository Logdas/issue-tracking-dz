#pragma once
#include "../Store/Store.h"
#include "../Screens/IScreen.h"
#include "../Screens/ID_Screens.h"
#include <stack>
using namespace std;

class App {
public:
	App();
	int start();
private:
	Store m_store;
	IScreen* m_current_screen;
	stack<IScreen*> m_stack_screens;

	bool m_is_run;
};

