#include "App.h"


App::App() {
	State test;
	m_store = Store::createStore(test);
	m_current_screen = m_store.getState().Map_Screens.at(IdScreen::START);
	m_is_run = true;
}

int App::start() {
	while (m_is_run) {
		IdScreen next_screen;
		next_screen = m_current_screen->start();
		if (next_screen == IdScreen::EXIT) {
			m_is_run = false;
		}
		else {
			m_current_screen = m_store.getState().Map_Screens.at(next_screen);
		}
	}
	return 0;
}
