#include "Store.h"
#include "../Models/Humans/Human.h"

#include "../Screens/Start/Start.h"
#include "../Screens/Auth/Auth.h"
#include "../Screens/Main/Main.h"
#include "../Screens/Detail_List/Detail_List.h"
#include "../Screens/Task_List/Task_List.h"
#include "../Screens/Detail_List/Detail_List.h"

#include <fstream>



Store* Store::s_store{};

Store::Store() {

}

Store::~Store() {
	//_saveState();
}

Store::Store(State state) {
	m_state = state;
}

Store& Store::createStore(State state) {
	static Store store(state);
	State defualt_state;
	defualt_state.Map_Screens.insert(std::make_pair(IdScreen::AUTHORIZATION, Auth::createScreen()));
	defualt_state.Map_Screens.insert(std::make_pair(IdScreen::START, Start::createScreen()));
	defualt_state.Map_Screens.insert(std::make_pair(IdScreen::MAIN, Main::createScreen()));
	defualt_state.Map_Screens.insert(std::make_pair(IdScreen::TASK_LIST, Task_List::createScreen()));
	defualt_state.Map_Screens.insert(std::make_pair(IdScreen::DETAIL_LIST, Detail_List::createScreen()));
	store.m_state = defualt_state;
	Human admin{ "admin", "admin" };
	store.dispatch(Action{ ActionTypes::ADD_NEW_USER, &admin });
	s_store = &store;
	return store;
}


Store* Store::getStore() {
	return s_store;
}

State Store::dispatch(Action action) {
	m_state = _reducer(action);
	return m_state;
}

State Store::getState() {
	return m_state;
}

State Store::_reducer(Action action) {
	State state(m_state);
	switch (action.types) {

	case ActionTypes::ADD_NEW_USER:
		state.List_Humans.push_back(new Human(*static_cast<Human*>(action.data)));
		break;
	case ActionTypes::DELETE_USER:
		try {
			if (*static_cast<int*>(action.data) >= state.List_Humans.size()) throw 1;
			else {
				delete state.List_Humans.at(*static_cast<int*>(action.data));
				state.List_Humans.erase(state.List_Humans.begin() + *static_cast<int*>(action.data));
			}
		}
		catch (int exception) {
			if (exception == 1) {
				cout << "������ �������� �� ����������. ����� ���������� ������� ENTER.";
				cin.clear();
				cin.ignore(323460, '\n');
				while (!InputEnter()) {};
			}
		}
		break;
	case ActionTypes::SET_CURRENT_USER:
		state.current_user = static_cast<Human*>(action.data);
		break;
	case ActionTypes::CLEAR_CURRENT_USER:
		state.current_user = nullptr;
		break;
	case ActionTypes::ADD_TASK:
		state.List_Tasks.push_back(new Task(*static_cast<Task*>(action.data)));
		break;
	case ActionTypes::SET_TASK:
		state.List_Tasks.at(*static_cast<int*>(action.data))->setStatus(true);
		break;
	case ActionTypes::ADD_DETAIL:
		state.List_Details.push_back(new Detail(*static_cast<Detail*>(action.data)));
		break;
	case ActionTypes::DEL_DETAIL:
		delete state.List_Details.at(*static_cast<int*>(action.data));
		state.List_Details.erase(state.List_Details.begin() + *static_cast<int*>(action.data));
		break;
	}
	return state;
}
