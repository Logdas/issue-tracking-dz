#pragma once
#include "../Models/Humans/Human.h"
#include "../Models/Details/Detail.h"
#include "../Models/Task/Task.h"
#include "../Screens/IScreen.h"

#include <vector>
#include <map>
using std::vector;
using std::map;

struct State {
	Human* current_user{ nullptr };
	vector<Human*>  List_Humans;
	vector<Detail*> List_Details;
	vector<Task*>   List_Tasks;
	map<const int, IScreen*> Map_Screens;
};