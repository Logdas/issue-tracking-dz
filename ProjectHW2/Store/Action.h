#pragma once
#include "ActionType.h"

struct Action {
	ActionTypes types;
	void* data;
};