#pragma once
#include "State.h"
#include "Action.h"

class Store {
public:
	Store();
	~Store();
	Store(State state);
	// ��������� ��������� ��������� � ���� � �������� ����� � �������
	static Store& createStore(State state);
	static Store* getStore();
	State dispatch(Action action);
	State getState();

private:

	State m_state{};

	static Store* s_store;

	State _reducer(Action action);
};