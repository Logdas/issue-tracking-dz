#pragma once

enum IdScreen : const int {
    AUTHORIZATION = 0,
    REGISTRATION,
    START,
    MAIN,
    DETAIL_LIST,
    TASK_LIST,
    EXIT
};