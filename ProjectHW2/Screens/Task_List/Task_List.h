#pragma once
#include "../IScreen.h"
#include "../../Store/Store.h"

class Task_List : public IScreen {
public:
	Task_List();
	~Task_List();
	IdScreen start() override;
	void render() override;
	static Task_List* createScreen();
private:
	Store* m_store;
	void addTask();
	void completeTask();
};

