#include "Task_List.h"

Task_List::Task_List() {

}

IdScreen Task_List::start() {
	m_store = Store::getStore();
	render();
	IdScreen Return_Screen{ IdScreen::START };
	if (m_store->getState().current_user != nullptr) {
		Return_Screen = IdScreen::MAIN;
		int choice{};
		cin >> choice;
		switch (choice) {
		case 1: addTask();      break;
		case 2: completeTask(); break;
		}
	}
	
	return Return_Screen;
}

void Task_List::render() {
	system("cls");

	if (m_store->getState().List_Tasks.size() != 0) {
		for (Task* i : m_store->getState().List_Tasks) {
			cout << *i;
		}
	}
	else cout << "����� ���� ��� ���";
	cout << endl;
	if (m_store->getState().current_user != nullptr) {
		cout << "1. �������� ������\n"
			<< "2. �������� ����������� ������\n";
	}
}

void Task_List::addTask() {
	Task NewTask;
	cin >> NewTask;
	cout << "�� ����������� �������? y/n ";
	char choice{};
	cin >> choice;
	if (choice == 'y') {
		NewTask.SetHuman(*m_store->getState().current_user);
	}
	else {
		cout << "\n�������� ������������ �������: \n";
		for (Human* it : m_store->getState().List_Humans) {
			cout << *it;
		}
		int number;
		cin >> number;
		--number;
		NewTask.SetHuman(*m_store->getState().List_Humans.at(number));
	}

	m_store->dispatch(Action{ ActionTypes::ADD_TASK, &NewTask });
}

void Task_List::completeTask() {
	system("cls");
	int j{1};

	if (m_store->getState().List_Tasks.size() != 0) {
		cout << "�������� ����������� ������: " << endl;

		for (Task* i : m_store->getState().List_Tasks) {
			//if (i->getStatus() != true) {
				cout << "----- " << j << " -----" << endl;
				cout << *i;
				++j;
			//}
		}
		cout << endl;
		int number;
		cin >> number;
		--number;
		m_store->dispatch(Action{ ActionTypes::SET_TASK, &number });
	} else cout << "� ������� �� ����������� ����� ���." << endl;
	while (!InputEnter());
}

Task_List* Task_List::createScreen() {
	static Task_List* auth = new Task_List();
	return auth;
}