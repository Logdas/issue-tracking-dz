#pragma once
#include <conio.h>
#include <iostream>
#include <limits>
#include <Windows.h>

#include "ID_Screens.h"
using namespace std;

#define ENTER 13

class IScreen {
public:
	virtual IdScreen start() = 0;
	virtual void render() = 0;
};

bool InputEnter();
int InputChoice(int range);