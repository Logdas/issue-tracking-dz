#pragma once
#include "../IScreen.h"
#include "../../Store/Store.h"

class Start : public IScreen {
public:
	Start();
	~Start();
	IdScreen start() override;
	void render() override;
	static Start* createScreen();
private:
	Store* m_store;
};
