#pragma once
#include "../IScreen.h"
#include "../../Store/Store.h"
#include "../../Models/Humans/Human.h"


class Auth : public IScreen {
public:
	Auth();
	~Auth();
	IdScreen start() override;
	void render() override;
	static Auth* createScreen();
private:
	Human* auth();
	Store* m_store;
};

