#include "IScreen.h"

bool InputEnter() {
	return _getch() == ENTER ? true : false;
}
int InputChoice(int range) {
	int choice{};
	try {
		cin >> choice;
		if (cin.fail()) {
			choice = -1;
			throw 1;
		}
		if (choice > range || choice < 0) {
			throw 2;
		}
	}
	catch (int exception) {
		switch (exception) {
		case 1:
			cout << "�� ����� �� �����. ���������� �����. ����� ���������� ������� ENTER.";
			cin.clear();
			cin.ignore(323460, '\n');
			while (!InputEnter()) {}; break;
		case 2:
			cout << "���������� ������ ���. ���������� �����. ����� ���������� ������� ENTER";
			cin.clear();
			cin.ignore(323460, '\n');
			while (!InputEnter()) {}; break;
		}
	}
	return choice;
}
