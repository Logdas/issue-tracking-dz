#include "Detail_List.h"

Detail_List::Detail_List() {

}

IdScreen Detail_List::start() {
	m_store = Store::getStore();
	render();
	IdScreen Return_Screen{ IdScreen::START };
	if (m_store->getState().current_user != nullptr) {
		Return_Screen = IdScreen::MAIN;
		int choice{};
		cin >> choice;
		switch (choice) {
		case 1: addDetail(); break;
		case 2: delDetail(); break;
		}
	}
	else while (!InputEnter());
	return Return_Screen;
}

void Detail_List::render() {
	system("cls");

	if (m_store->getState().List_Details.size() != 0) {
		for (Detail* i : m_store->getState().List_Details) {
			cout << *i << endl;
		}
	}
	else cout << "������ ���";
	cout << endl;
	if (m_store->getState().current_user != nullptr) {
		cout << "1. �������� ������\n"
			<< "2. ������� ������\n";
	}
}

Detail_List* Detail_List::createScreen() {
	static Detail_List* auth = new Detail_List();
	return auth;
}

void Detail_List::addDetail() {
	Detail NewDetail;
	cin >> NewDetail;
	m_store->dispatch(Action{ ActionTypes::ADD_DETAIL, &NewDetail });
}

void Detail_List::delDetail() {
	int j{1};
	for (Detail* i : m_store->getState().List_Details) {
		cout << "----- " << j << " -----\n";
		cout << *i << endl;
		j++;
	}
	cout << "\n������� ����� ������, ������� ������ �������: ";
	int number;
	cin >> number;
	--number;
	m_store->dispatch(Action{ ActionTypes::DEL_DETAIL, &number });
}
