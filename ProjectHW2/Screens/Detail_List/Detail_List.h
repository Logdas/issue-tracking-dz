#pragma once
#include "../IScreen.h"
#include "../../Store/Store.h"

class Detail_List : public IScreen {
public:
	Detail_List();
	~Detail_List();
	IdScreen start() override;
	void render() override;
	static Detail_List* createScreen();
private:
	Store* m_store;
	void addDetail();
	void delDetail();
};

