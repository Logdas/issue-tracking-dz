#pragma once
#include "../../Store/Store.h"
#include "../IScreen.h"

enum Types {
	HUMAN = 0,
	SESSION
};

class Main : public IScreen {
public:
	Main();
	IdScreen start() override;
	void render() override;
	static Main* createScreen();
private:
	template<class T>
	void Print(vector<T> vec) {
		for (T it : vec) {
			cout << *it;
		}
	}
	void AddEmployee();
	void DelEmployee();
	void PrintTask();
	Store* m_store;
};

