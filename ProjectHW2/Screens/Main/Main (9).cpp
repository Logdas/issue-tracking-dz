#include "Main.h"
#include <algorithm>

Main::Main() {

}

IdScreen Main::start() {

	IdScreen Return_Screen{ IdScreen::MAIN };
	bool exit{ false };
	while (!exit) {
		m_store = Store::getStore();
		render();
		vector<Human*> temp = m_store->getState().List_Humans;
		vector<Task*> temp2 = m_store->getState().List_Tasks;

		switch (InputChoice(8)) {
		case 1: Print(m_store->getState().List_Humans); while (!InputEnter()); break;
		case 2: AddEmployee(); break;
		case 3: DelEmployee(); break;
		case 4: PrintTask();   break;
		case 5: Return_Screen = IdScreen::DETAIL_LIST; exit = true; break;
		case 6: Return_Screen = IdScreen::TASK_LIST;   exit = true; break;
		case 0: Return_Screen = IdScreen::START; m_store->dispatch(Action{ ActionTypes::CLEAR_CURRENT_USER }); exit = true; break;

		}

	}
	return Return_Screen;
}

void Main::render() {
	system("cls");

	cout << "1. ������ �����������.\n"
		<< "2. �������� ����������.\n"
		<< "3. ������� ����������.\n"

		<< "4. ����������� ����������� ������.\n"
		<< "5. ��������/������� ������.\n"
		<< "6. ��������/�������� �����.\n"

		<< "0. �����\n"
		<< "-> ";
}

Main* Main::createScreen() {
	static Main* auth = new Main();
	return auth;
}

void Main::AddEmployee() {
	system("cls");
	Human admin;
	admin.set();
	admin.setID(Human::s_idGenerator++);
	m_store->dispatch(Action{ ActionTypes::ADD_NEW_USER, &admin });
}

void Main::DelEmployee() {
	system("cls");
	Print(m_store->getState().List_Humans);
	int number_delete;
	cin >> number_delete;
	--number_delete;
	m_store->dispatch(Action{ ActionTypes::DELETE_USER, &number_delete });
}

void Main::PrintTask() {
	cout << "���� ������: " << endl;
	for (Task* i : m_store->getState().List_Tasks) {
		if (i->GetHuman() == *m_store->getState().current_user) {
			cout << *i;
		}
	}
	while (!InputEnter());
}

